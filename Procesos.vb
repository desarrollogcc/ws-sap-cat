﻿Imports Microsoft.VisualBasic
Imports SAP.Middleware.Connector
Imports System.Data

Public Class Procesos

    Public Shared Function conectarSAP(ByVal Conexion As String) As RfcConfigParameters
        Dim SapParametros As New RfcConfigParameters
        Dim Conexiones() As String = Conexion.Split("|")
        Dim SAPUSER, SAPPASS, SAPSYST, SAPCLNT, SAPNAME, SAPHOST, SAPNUMB, SAPLANG As String

        For i As Integer = 0 To Conexiones.Length - 1
            Dim Aux() As String = Conexiones(i).Split("=")
            If Aux(0).Trim.ToUpper = "HOST" Then
                SAPHOST = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "USER" Then
                SAPUSER = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "PASS" Then
                SAPPASS = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "CLNT" Then
                SAPCLNT = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "NUMB" Then
                SAPNUMB = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "LANG" Then
                SAPLANG = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "SYST" Then
                SAPSYST = Aux(1)
            ElseIf Aux(0).Trim.ToUpper = "NAME" Then
                SAPNAME = Aux(1)
            End If
        Next

        SapParametros(RfcConfigParameters.User) = SAPUSER
        SapParametros(RfcConfigParameters.Password) = SAPPASS
        SapParametros(RfcConfigParameters.SystemID) = SAPSYST
        SapParametros(RfcConfigParameters.Client) = SAPCLNT
        SapParametros(RfcConfigParameters.Name) = SAPNAME
        SapParametros(RfcConfigParameters.AppServerHost) = SAPHOST
        SapParametros(RfcConfigParameters.SystemNumber) = SAPNUMB
        SapParametros(RfcConfigParameters.Language) = SAPLANG
        Return SapParametros
    End Function

    Public Shared Sub ReturnParamConfig(ByVal Metodo As String, ByRef FuncionEjecutar As String, ByRef Retorno As String, ByRef Entrada As String)
        Dim Bapi As String() = ConfigurationManager.AppSettings(Metodo).Split(".")
        FuncionEjecutar = Bapi(0)
        Retorno = Bapi(1)
        Entrada = Bapi(2)
    End Sub

    Public Shared Sub ReturnParamConfigAPT(ByVal Metodo As String, ByRef FuncionEjecutar As String, ByRef Retorno As String)
        Dim Bapi As String() = ConfigurationManager.AppSettings(Metodo).Split(".")
        FuncionEjecutar = Bapi(0)
        Retorno = Bapi(1)
    End Sub

    Public Shared Sub ReturnParamConfig2(ByVal Metodo As String, ByRef FuncionEjecutar As String, ByRef Retorno As String)
        Dim Bapi As String() = ConfigurationManager.AppSettings(Metodo).Split(".")
        FuncionEjecutar = Bapi(0)
        Retorno = Bapi(1)
    End Sub

    Public Shared Sub ReturnParamConfigSaldoPendiente(ByVal Metodo As String, ByRef FuncionEjecutar As String, ByRef Entrada As String, ByRef Entrada2 As String, ByRef Entrada3 As String, ByRef Retorno As String, ByRef Retorno2 As String, ByRef Retorno3 As String, ByRef Retorno4 As String, ByRef Retorno5 As String)
        Dim Bapi As String() = ConfigurationManager.AppSettings(Metodo).Split(".")
        FuncionEjecutar = Bapi(0)
        Entrada = Bapi(1)
        Entrada2 = Bapi(2)
        Entrada3 = Bapi(3)
        Retorno = Bapi(4)
        Retorno2 = Bapi(5)
        Retorno3 = Bapi(6)
        Retorno4 = Bapi(7)
        Retorno5 = Bapi(8)
    End Sub

    Public Shared Function EjecutarFuncionSAP(ByVal SapParametros As RfcConfigParameters, ByVal NombreFuncion As String, ByVal TipoRetorno As String, ByVal Retorno As String, ByVal Parametros() As String, ByVal ValoresParametros() As String)
        'Conexión SAP
        Dim destino As RfcDestination

        destino = RfcDestinationManager.GetDestination(SapParametros)

        'Bapi o Funcion de SAP
        Dim funcion As IRfcFunction = destino.Repository.CreateFunction(NombreFuncion)
        'Parametros de la Bapi o funcion de SAP
        For i As Integer = 0 To Parametros.Length - 1
            funcion.SetValue(Parametros(i).ToUpper, ValoresParametros(i))
        Next

        'Ejecucion de SAP
        funcion.Invoke(destino)

        If TipoRetorno = "T" Then 'Tabla
            Return GetTableFromSAP(Retorno, funcion)
        Else 'Parametro Exporting
            Return funcion.GetString(Retorno)
        End If
    End Function

    Public Shared Function GetTableFromSAP(ByVal TablaRetorno As String, ByVal Funcion As IRfcFunction) As DataSet
        'Instanciación de regreso SAP
        Dim TablaDatos As IRfcTable = Funcion.GetTable(TablaRetorno.ToUpper)

        ' convierte la tabla de SAP a tabla .NET
        Dim TablaSAP As New DataTable

        For liElement As Integer = 0 To TablaDatos.ElementCount - 1
            Dim metadata As RfcElementMetadata = TablaDatos.GetElementMetadata(liElement)
            TablaSAP.Columns.Add(metadata.Name)
        Next

        For Each row As IRfcStructure In TablaDatos
            Dim sapdr As DataRow = TablaSAP.NewRow()
            For liElement As Integer = 0 To TablaDatos.ElementCount - 1
                Dim metadata As RfcElementMetadata = TablaDatos.GetElementMetadata(liElement)
                sapdr(metadata.Name) = row.GetString(metadata.Name)
            Next
            TablaSAP.Rows.Add(sapdr)
        Next

        'Convierte tabla de salida
        Dim TablaResultado As New DataSet
        TablaResultado.Tables.Add(TablaSAP)

        Return TablaResultado
    End Function

    Public Shared Function GetStructureFromTable(ByVal Destino As RfcDestination, ByVal Funcion As IRfcFunction, ByVal TablaFuncion As String) As IRfcStructure
        Dim RfcTable As IRfcTable = Funcion.GetTable(TablaFuncion)
        Dim TableMetaData As RfcTableMetadata = RfcTable.Metadata
        Dim StructureMetadata As RfcStructureMetadata = TableMetaData.LineType
        Dim NombreEstructura As String = StructureMetadata.Name
        Dim Estructura As IRfcStructure = Destino.Repository.GetStructureMetadata(NombreEstructura).CreateStructure()
        Return Estructura
    End Function

    Public Shared Function DataTable(ByVal sentencia As String, ByVal Conexion As String, Optional ByVal param() As SqlClient.SqlParameter = Nothing, Optional ByVal Tipo As String = "SP") As DataTable
        'Te Retorna un DataSet
        Dim con As New SqlClient.SqlConnection
        Try
            Dim comando As New SqlClient.SqlCommand
            Dim adap As New SqlClient.SqlDataAdapter
            Dim dset As New DataSet
            con.ConnectionString = Conexion
            con.Open()
            comando.Connection = con
            comando.CommandTimeout = 0
            If Tipo = "SP" Then
                comando.CommandType = CommandType.StoredProcedure
            Else
                comando.CommandType = CommandType.Text
            End If
            comando.CommandText = sentencia
            If param IsNot Nothing Then
                For i As Integer = 0 To param.Length - 1
                    comando.Parameters.Add(param(i))
                Next
            End If
            adap.SelectCommand = comando
            adap.Fill(dset)
            Return dset.Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            con.Close()
        End Try
    End Function

    Public Shared Function ExecuteNonQuery(ByVal sentencia As String, ByVal Conexion As String, Optional ByVal param() As SqlClient.SqlParameter = Nothing, Optional ByVal Tipo As String = "SP") As String
        'Ejecuta una sentencia
        Dim Err As String = ""
        Dim con As New SqlClient.SqlConnection
        Try
            Dim comando As New SqlClient.SqlCommand
            Dim adap As New SqlClient.SqlDataAdapter
            Dim dt As New DataTable
            con.ConnectionString = Conexion
            con.Open()
            comando.Connection = con
            If Tipo = "SP" Then
                comando.CommandType = CommandType.StoredProcedure
            Else
                comando.CommandType = CommandType.Text
            End If
            comando.CommandText = sentencia
            If param IsNot Nothing Then
                For i As Integer = 0 To param.Length - 1
                    comando.Parameters.Add(param(i))
                Next
            End If
            comando.ExecuteNonQuery()
        Catch ex As Exception
            'Err = ex.Message
            Throw New Exception(ex.Message)
        Finally
            con.Close()
        End Try

        Return Err
    End Function
End Class
