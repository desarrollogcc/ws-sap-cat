﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SAP.Middleware.Connector

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService
    Dim ConexionSAP As String = ConfigurationManager.AppSettings("cnnsap")
    Dim FuncionEjecutar As String = ""
    Dim Retorno As String = ""
    Dim Retorno2 As String = ""
    Dim Retorno3 As String = ""
    Dim Retorno4 As String = ""
    Dim Retorno5 As String = ""
    Dim Entrada As String
    Dim Entrada2 As String
    Dim Entrada3 As String
    Dim dt As DataSet

    <WebMethod()>
    Public Function SAP_APT() As DataSet

        Dim IT_APERTURAS As DataSet


        Dim SapParametros As New RfcConfigParameters
        Procesos.ReturnParamConfigAPT("SAP_APT", FuncionEjecutar, Retorno)

        'Se Conecta a SAP
        SapParametros = Procesos.conectarSAP(ConexionSAP)
        Dim destino As RfcDestination = RfcDestinationManager.GetDestination(SapParametros)

        'Bapi o Funcion de SAP
        Dim funcion As IRfcFunction = destino.Repository.CreateFunction(FuncionEjecutar)

        'Ejecucion de SAP
        funcion.Invoke(destino)
        'Dim s As String = funcion.GetString(Retorno)
        IT_APERTURAS = Procesos.GetTableFromSAP(Retorno, funcion)

        Return IT_APERTURAS

    End Function

End Class