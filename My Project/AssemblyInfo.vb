﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie los valores de estos atributos para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado
<Assembly: AssemblyTitle("WS_MonitorHCMFI")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("WS_MonitorHCMFI")>
<Assembly: AssemblyCopyright("Copyright ©  2019")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'El siguiente GUID es para el Id. typelib cuando este proyecto esté expuesto a COM
<Assembly: Guid("fbe94bce-5521-4870-8363-6721f87e3fd5")>

' La información de versión de un ensamblado consta de los siguientes cuatro valores:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' Puede especificar todos los valores o aceptar los valores predeterminados de los números de compilación y de revisión 
' mediante el carácter '*', como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
